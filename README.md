## Balanceo de cargas de alta disponibilidad

En esta practica se va a trabajar con Nginx, Ansible y Docker Compose para simular un balanceo de cargas entre servidores

Se van a tener 5 servidores, uno que funciona con el Nginx (Frontal), este va a ser el encargado de ver
que es lo que quiere el cliente y mandarlo a alguno de los servidores disponibles, los servidores cuentan
con 3 instancias de node. 

Cada servidor (servidor-web-1, servidor-web-2, servidor web-3) va a ser configurado mediante un playbook y
contara con 3 instancias de Node y un Nginx.

Un Bastion que contara con Ansible, de esta manera se usaran playbooks que permitan instalar Docker dentro
de los contenedores asi como git para (intentar) intentar sacar el DinD y los materiales necesarios para
que cuenten con Nginx y los Node.

Un ejemplo en diagrama de lo anterior

![Diagrama](/BalanceoCargas.png "Balanceo de Cargas, representacion")
